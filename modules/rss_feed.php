<?php

/**
 * @author Axl Hoogelander
 * @copyright 2012
 */

class RSS_Feed{
    
    private $url;
    
    public function __construct($url){
        $this->url = $url;
    }
    
    public function xml()
    {
        return simplexml_load_file($this->url);
    }
    
    public function title(){
        return self::xml()->channel->title;
    }
    
    public function content(){
        return self::xml()->channel->item;
    }
    
}

?>