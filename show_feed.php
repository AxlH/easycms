<?php

/**
 * @author Axl Hoogelander
 * @copyright 2012
 */
 
 include './modules/rss_feed.php';

$feed = new RSS_Feed('http://bitbucket.org/AxlH/easycms/rss');

echo $feed->title();

foreach($feed->content() as $node):?>
    
     <div style="color: #aaa; padding: 5px; width: 440px; min-height: 20px; background-color: #ebebeb; border-left: 3px solid #ccc; margin-top: 5px;">
            <span style="text-decoration: underline; color: #777;"> 
                <?php echo $node->title?>
            </span>
            <br />
            <span style="font-size: 12px; color: #999">
                <?php echo str_replace("+0000", null, $node->pubDate)?>
            </span>
            <br />
            
                <?php echo str_replace('<a ', '<a style="color: #666;"',$node->description)?>
       
       
       </div>
<?php endforeach;?>